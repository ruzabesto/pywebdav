# -*- coding: utf-8 -*-

import unittest
import pywebdav
import bottle
import xml.etree.ElementTree as ET


class TestXMLNameSpace(unittest.TestCase):
    def test_wo_prefix(self):
        ns = pywebdav.XMLNameSpace("test")
        self.assertEqual(str(ns), "XMLNameSpace(:test)")
        self.assertEqual(ns.prefix, "")
        self.assertEqual(ns.as_dict(), {"xmlns": "test"})

    def test_with_prefix(self):
        ns = pywebdav.XMLNameSpace("test", "t")
        self.assertEqual(str(ns), "XMLNameSpace(t:test)")
        self.assertEqual(ns.prefix, "t")
        self.assertEqual(ns.as_dict(), {"xmlns:t": "test"})

    def test_cmp(self):
        ns1 = pywebdav.XMLNameSpace("test")
        ns2 = pywebdav.XMLNameSpace("test")
        self.assertTrue(ns1 == ns2)

        ns1 = pywebdav.XMLNameSpace("test")
        ns2 = pywebdav.XMLNameSpace("test2")
        self.assertFalse(ns1 == ns2)

        ns1 = pywebdav.XMLNameSpace("test")
        ns2 = pywebdav.XMLNameSpace("test", "t")
        self.assertTrue(ns1 == ns2)

        ns1 = pywebdav.XMLNameSpace("test", "x")
        ns2 = pywebdav.XMLNameSpace("test", "t")
        self.assertTrue(ns1 == ns2)


class TestWebDAVProperty(unittest.TestCase):
    def test_xml_wo_ns(self):
        prop = pywebdav.WebDAVProperty("test")
        parent = ET.Element("p")
        xml = ET.tostring(prop.xmlElement(parent))
        self.assertEqual(xml, b"<test />")
        self.assertEqual(ET.tostring(parent), b"<p><test /></p>")

    def test_xml_with_ns(self):
        prop = pywebdav.WebDAVProperty("test", pywebdav.XMLNameSpace("NS", "n"))
        parent = ET.Element("p")
        xml = ET.tostring(prop.xmlElement(parent))
        self.assertEqual(xml, b'<n:test xmlns:n="NS" />')
        self.assertEqual(ET.tostring(parent), b'<p><n:test xmlns:n="NS" /></p>')

    def test_cmp(self):
        p1 = pywebdav.WebDAVProperty("test")
        p2 = pywebdav.WebDAVProperty("test")
        self.assertTrue(p1 == p2)

        p1 = pywebdav.WebDAVProperty("test")
        p2 = pywebdav.WebDAVProperty("test", pywebdav.XMLNameSpace("NS", "n"))
        self.assertFalse(p1 == p2)

        p1 = pywebdav.WebDAVProperty("test", pywebdav.XMLNameSpace("NS", "n"))
        p2 = pywebdav.WebDAVProperty("test", pywebdav.XMLNameSpace("NS", "n"))
        self.assertTrue(p1 == p2)

        p1 = pywebdav.WebDAVProperty("test", pywebdav.XMLNameSpace("NS", "n"))
        p2 = pywebdav.WebDAVProperty("test", pywebdav.XMLNameSpace("NS"))
        self.assertTrue(p1 == p2)


class TestWebDAVBaseExecutor(unittest.TestCase):
    def test_wrong_listener(self):
        with self.assertRaises(TypeError):
            pywebdav.WebDAVBaseExecutor(listener="hello")

    def test_wrong_command(self):
        e = pywebdav.WebDAVBaseExecutor()
        res = e("test", 1, 2, 3)
        self.assertEqual(res, None)

    def test_listener_replace_func(self):
        class TestListener(pywebdav.WebDAVCommandListener):
            def replace_test(self, original_func, *args):
                return "replaced_result(%s)" % (args,)

        e = pywebdav.WebDAVBaseExecutor(listener=TestListener())

        def test_func(*args):
            return "original_result(%s)" % (args,)

        e.test = test_func
        res = e("test", 1, 2, 3)
        self.assertEqual(res, "replaced_result((1, 2, 3))")

    def test_listener_before_allow(self):
        class TestListener(pywebdav.WebDAVCommandListener):
            def before_test(self, *args):
                return True

        e = pywebdav.WebDAVBaseExecutor(listener=TestListener())

        def test_func(*args):
            return "result(%s)" % (args,)

        e.test = test_func
        res = e("test", 1, 2, 3)
        self.assertEqual(res, "result((1, 2, 3))")

    def test_listener_before_deny(self):
        class TestListener(pywebdav.WebDAVCommandListener):
            def before_test(self, *args):
                return False

        e = pywebdav.WebDAVBaseExecutor(listener=TestListener())

        def test_func(*args):
            return "result(%s)" % (args,)

        e.test = test_func
        res = e("test", 1, 2, 3)
        self.assertEqual(res, None)

    def test_listener_common_before_allow(self):
        class TestListener(pywebdav.WebDAVCommandListener):
            def before(self, command, *args):
                assert command == "test"
                return True

        e = pywebdav.WebDAVBaseExecutor(listener=TestListener())

        def test_func(*args):
            return "result(%s)" % (args,)

        e.test = test_func
        res = e("test", 1, 2, 3)
        self.assertEqual(res, "result((1, 2, 3))")

    def test_listener_common_before_deny(self):
        class TestListener(pywebdav.WebDAVCommandListener):
            def before(self, command, *args):
                assert command == "test"
                return False

        e = pywebdav.WebDAVBaseExecutor(listener=TestListener())

        def test_func(*args):
            return "result(%s)" % (args,)

        e.test = test_func
        res = e("test", 1, 2, 3)
        self.assertEqual(res, None)


class TestWebDAVServerBase(unittest.TestCase):
    def test_init_with_executor(self):
        app = bottle.Bottle()
        e = pywebdav.WebDAVBaseExecutor()
        base = pywebdav.WebDAVServer(
            app, "/webdav", "test_data/server_home", executor=e
        )
        self.assertEqual(e, base.executor)

# -*- coding: utf-8 -*-

import unittest
import webdav3
from webdav3.client import Client
from urllib.request import urlopen
from urllib.error import HTTPError


def start_server():
    import threading
    import bottle
    from pywebdav import (
        WebDAVServer,
        WebDAVCommandListener,
        WebDAVProperty,
        XMLNameSpace,
    )

    class TestListener(WebDAVCommandListener):
        TEST_PROPERTY = WebDAVProperty("testproperty", XMLNameSpace("TEST:", "t"))

        def after_getpropnames(self, res):
            res.append(self.TEST_PROPERTY)
            return res

        def after_getproperties(self, res, filename, url_name, props):
            if self.TEST_PROPERTY in props:
                res[self.TEST_PROPERTY] = "test property value"
            return res

    app = bottle.Bottle()
    WebDAVServer(app, "/webdav", "test_data/server_home", listener=TestListener())

    @app.route("/")
    def index():
        return "hello"

    def wait_serverv_start():
        import time

        total_sleep = 0
        interval = 0.005
        while True:
            try:
                res = urlopen("http://localhost:8000/")
                break
            except:
                time.sleep(interval)
            time.sleep(interval)
            total_sleep += interval
        print("Server started in %.3f seconds" % total_sleep)

    class ServerThread(threading.Thread):
        def run(self):
            app.run(host="0.0.0.0", port=8000, quiet=True)

    server_thread = ServerThread()
    server_thread.setDaemon(True)
    server_thread.start()
    wait_serverv_start()


def create_client():
    options = {
        "webdav_hostname": "http://localhost:8000/webdav"
        # 'webdav_login':    "login",
        # 'webdav_password': "password"
    }

    client = Client(options)

    from webdav3.client import (
        WebDavXmlUtils,
        wrap_connection_error,
        RemoteResourceNotFound,
    )
    from webdav3.urn import Urn
    import lxml.etree as etree

    @wrap_connection_error
    def internal_get_prop_list(client, remote_path):
        urn = Urn(remote_path)
        if not client.check(urn.path()):
            raise RemoteResourceNotFound(urn.path())

        data = WebDavXmlUtils.create_get_property_request_content(
            dict(name="propname", namespace="DAV:")
        )
        response = client.execute_request(
            action="get_property", path=urn.quote(), data=data
        )
        tree = etree.fromstring(response.content)
        res = []
        for node in tree.xpath('//*[local-name() = "propstat"]/*'):
            res.append(node.tag)
        return res

    client.get_proplist = internal_get_prop_list
    return client


class TestWebDAV(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        start_server()

    def setUp(self):
        self.client = create_client()

    def supress_exc(self, func, *params):
        try:
            func(*params)
        except Exception as e:
            pass

    def tearDown(self):
        self.supress_exc(self.client.clean, "/test2/test_copy.txt")
        self.supress_exc(self.client.clean, "/test3/test_copy.txt")
        self.supress_exc(self.client.clean, "/test2")
        self.supress_exc(self.client.clean, "/test3")
        self.supress_exc(self.client.clean, "/test4")
        self.supress_exc(self.client.clean, "test_upload.txt")
        self.supress_exc(self.client.clean, "/test/test_upload.txt")
        self.supress_exc(self.client.clean, "/test/test_copy.txt")

    def test_get_root(self):
        res = urlopen("http://localhost:8000/webdav/").read()
        self.assertTrue(b"<!DOCTYPE html>" in res)
        self.assertTrue(b"<a href" in res)
        self.assertTrue(b"/1.txt" in res)
        self.assertTrue(b"/test" in res)

    def test_get_path(self):
        res = urlopen("http://localhost:8000/webdav/test/").read()
        self.assertTrue(b"<!DOCTYPE html>" in res)
        self.assertTrue(b"<a href" in res)
        self.assertTrue(b"/test/dalek.jpeg" in res)

    def test_get_file(self):
        res = urlopen("http://localhost:8000/webdav/1.txt").read()
        self.assertTrue(b"1" in res)

    def test_check_root(self):
        res = self.client.check("/")
        self.assertTrue(res)

    def test_check_file(self):
        res = self.client.check("/1.txt")
        self.assertTrue(res)

    def test_check_dir1(self):
        res = self.client.check("/test")
        self.assertTrue(res)

    def test_check_dir2(self):
        res = self.client.check("/test/")
        self.assertTrue(res)

    def test_check_non_existing(self):
        res = self.client.check("/test555")
        self.assertFalse(res)

    def test_list_root(self):
        res = self.client.list()
        # self.assertTrue("/" in res)
        ## instead of "/" it returns "webdav/", looks like a bug in lib :-)
        self.assertTrue("test/" in res)
        self.assertTrue("1.txt" in res)

    def test_list_root_with_details(self):
        res = self.client.list(get_info=True)
        res_root = next(filter(lambda x: x["name"] == "/webdav", res))
        self.assertEqual(res_root["name"], "/webdav")
        self.assertEqual(res_root["path"], "/webdav")
        self.assertTrue(res_root["size"] is not None)
        res_test = next(filter(lambda x: "test" in x["name"], res))
        self.assertEqual(res_test["name"], "/webdav/test")
        self.assertEqual(res_test["path"], "/webdav/test")
        self.assertTrue(res_test["size"] is not None)
        res_1txt = next(filter(lambda x: "1.txt" in x["name"], res))
        self.assertEqual(res_1txt["name"], "/webdav/1.txt")
        self.assertEqual(res_1txt["path"], "/webdav/1.txt")
        self.assertEqual(res_1txt["size"], "2")

    def test_mkdir(self):
        res = self.client.mkdir("test2")
        res = self.client.info("/test2")
        self.assertTrue(res["created"] is not None)
        self.assertTrue(res["name"] is not None)
        self.assertTrue(res["size"] is not None)
        self.assertTrue(res["modified"] is not None)
        self.assertTrue(res["etag"] is not None)

    def test_rmdir(self):
        self.client.mkdir("test3")
        self.client.clean("test3")
        with self.assertRaises(webdav3.exceptions.RemoteResourceNotFound):
            self.client.info("/test3")

    def test_mkdir_already_exists(self):
        # webdav client issue, server should return 405 error, client supress it
        res = self.client.mkdir("test")

    def test_mkdir_in_non_existing_one(self):
        with self.assertRaises(webdav3.exceptions.RemoteParentNotFound):
            self.client.mkdir("test4/555/")

    def test_info_dir(self):
        res = self.client.info("/test")
        self.assertTrue(res["created"] is not None)
        self.assertTrue(res["name"] is not None)
        self.assertTrue(res["size"] is not None)
        self.assertTrue(res["modified"] is not None)
        self.assertTrue(res["etag"] is not None)

    def test_list_dir(self):
        res = self.client.list("/test")
        self.assertTrue("dalek.jpeg" in res)

    def test_list_dir_with_details(self):
        res = self.client.list("/test", get_info=True)
        res_dalek = next(filter(lambda x: "dalek" in x["name"], res))
        self.assertEqual(res_dalek["name"], "/webdav/test/dalek.jpeg")
        self.assertEqual(res_dalek["path"], "/webdav/test/dalek.jpeg")
        self.assertEqual(res_dalek["size"], "2021")
        res_dir = next(filter(lambda x: x["name"] == "/webdav/test", res))
        self.assertTrue(res_dir["size"] is not None)
        self.assertEqual(res_dir["name"], "/webdav/test")
        self.assertEqual(res_dir["path"], "/webdav/test")

    def test_info_file(self):
        res = self.client.info("/1.txt")
        self.assertTrue(res["created"] is not None)
        self.assertTrue(res["name"] is not None)
        self.assertTrue(res["size"] is not None)
        self.assertTrue(res["modified"] is not None)
        self.assertTrue(res["etag"] is not None)

    def test_info_root(self):
        res = self.client.info("/")
        self.assertTrue(res["created"] is not None)
        self.assertTrue(res["name"] is not None)
        self.assertTrue(res["size"] is not None)
        self.assertTrue(res["modified"] is not None)
        self.assertTrue(res["etag"] is not None)

    def test_free_space(self):
        res = self.client.free()
        self.assertTrue(int(res) > 0)

    def test_upload(self):
        self.client.upload_sync("test_upload.txt", "test_data/to_upload/2.txt")
        res = self.client.info("/test_upload.txt")
        self.assertTrue(res["created"] is not None)
        self.assertTrue(res["name"] is not None)
        self.assertTrue(res["size"] == "35")
        self.assertTrue(res["modified"] is not None)
        self.assertTrue(res["etag"] is not None)

    def test_upload_path(self):
        self.client.upload_sync("/test/test_upload.txt", "test_data/to_upload/2.txt")
        res = self.client.info("/test/test_upload.txt")
        self.assertTrue(res["created"] is not None)
        self.assertTrue(res["name"] is not None)
        self.assertTrue(res["size"] == "35")
        self.assertTrue(res["modified"] is not None)
        self.assertTrue(res["etag"] is not None)

    def test_rmfile(self):
        self.client.upload_sync("/test/test_upload.txt", "test_data/to_upload/2.txt")
        self.client.clean("/test/test_upload.txt")
        with self.assertRaises(webdav3.exceptions.RemoteResourceNotFound):
            self.client.info("/test/test_upload.txt")

    def test_copy_file(self):
        self.client.upload_sync("/test/test_upload.txt", "test_data/to_upload/2.txt")
        self.client.copy(
            remote_path_from="/test/test_upload.txt",
            remote_path_to="/test/test_copy.txt",
        )
        res = self.client.info("/test/test_copy.txt")
        self.assertTrue(res["created"] is not None)
        self.assertTrue(res["name"] is not None)
        self.assertTrue(res["size"] == "35")
        self.assertTrue(res["modified"] is not None)
        self.assertTrue(res["etag"] is not None)

    def test_copy_dir(self):
        self.client.mkdir("test2")
        self.client.upload_sync("/test2/test_copy.txt", "test_data/to_upload/2.txt")
        self.client.copy(remote_path_from="/test2", remote_path_to="/test3", depth=0)
        res = self.client.info("/test3")
        self.assertTrue(res["created"] is not None)
        self.assertTrue(res["name"] is not None)
        self.assertTrue(res["size"] is not None)
        self.assertTrue(res["modified"] is not None)
        self.assertTrue(res["etag"] is not None)

    def test_copy_dir_recursive(self):
        self.client.mkdir("test2")
        self.client.upload_sync("/test2/test_copy.txt", "test_data/to_upload/2.txt")
        self.client.copy(
            remote_path_from="/test2", remote_path_to="/test3", depth="infinity"
        )
        res = self.client.info("/test3")
        self.assertTrue(res["created"] is not None)
        self.assertTrue(res["name"] is not None)
        self.assertTrue(res["size"] is not None)
        self.assertTrue(res["modified"] is not None)
        self.assertTrue(res["etag"] is not None)
        res = self.client.info("/test3/test_copy.txt")
        self.assertTrue(res["created"] is not None)
        self.assertTrue(res["name"] is not None)
        self.assertTrue(res["size"] == "35")
        self.assertTrue(res["modified"] is not None)
        self.assertTrue(res["etag"] is not None)

    def test_get_property(self):
        res = self.client.get_property("1.txt", dict(name="getetag"))
        self.assertTrue(res is not None)

    def test_get_property_ns(self):
        res = self.client.get_property("1.txt", dict(namespace="DAV:", name="getetag"))
        self.assertTrue(res is not None)

    def test_get_test_property(self):
        res = self.client.get_property(
            "1.txt", dict(name="testproperty", namespace="TEST:")
        )
        self.assertEqual(res, "test property value")

    def test_get_proplist(self):
        res = self.client.get_proplist(self.client, "1.txt")
        self.assertTrue("{DAV:}creationdate" in res)
        self.assertTrue("{DAV:}getcontenttype" in res)
        self.assertTrue("{DAV:}getetag" in res)
        self.assertTrue("{DAV:}getlastmodified" in res)
        self.assertTrue("{TEST:}testproperty" in res, res)

    def test_info_non_existed_file(self):
        with self.assertRaises(webdav3.exceptions.RemoteResourceNotFound):
            self.client.info("/test555")

    def test_get_non_existed_file(self):
        with self.assertRaises(HTTPError):
            urlopen("http://localhost:8000/webdav/5.txt")


if __name__ == "__main__":
    unittest.main()

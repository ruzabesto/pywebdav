# -*- coding: utf-8 -*-

import bottle
import xml.etree.ElementTree as ET
import os, time
import urllib.parse
import shutil

__all__ = ["WebDAVServer", "WebDAVExecutor"]


class XMLNameSpace(object):
    def __init__(self, name, prefix=""):
        self.name = name
        self.prefix = prefix

    def __eq__(self, other):
        return self.name == other.name

    def __repr__(self):
        return "XMLNameSpace(%s:%s)" % (self.prefix, self.name)

    def as_dict(self):
        if self.prefix:
            return {"xmlns:%s" % self.prefix: self.name}
        else:
            return {"xmlns": self.name}


class WebDAVProperty(object):
    def __init__(self, name, ns=None):
        self.name = name
        self.ns = ns

    def __xml_tag__(self):
        if self.ns and self.ns.prefix:
            return "%s:%s" % (self.ns.prefix, self.name)
        else:
            return self.name

    def xmlElement(self, parent):
        if self.ns:
            return ET.SubElement(parent, self.__xml_tag__(), self.ns.as_dict())
        else:
            return ET.SubElement(parent, self.__xml_tag__())

    def __eq__(self, other):
        if self.ns and other.ns:
            return self.name == other.name and self.ns == other.ns
        elif self.ns or other.ns:
            return False
        else:
            return self.name == other.name

    def __repr__(self):
        if self.ns:
            return "WebDAVProperty({%s}%s)" % (self.ns.name, self.name)
        else:
            return "WebDAVProperty(%s)" % self.name

    def __hash__(self):
        return hash(self.__repr__())

    @classmethod
    def parse(cls, tag):
        if "}" in tag:
            ns = tag.split("}")[0].split("{")[1]
            name = tag.split("}")[1]
            return WebDAVProperty(name, XMLNameSpace(ns))
        else:
            return WebDAVProperty(tag)


DAV_NS = XMLNameSpace("DAV:", "d")
COMMON_PROPS = [
    WebDAVProperty("quota-available-bytes", DAV_NS),
    WebDAVProperty("quota-used-bytes", DAV_NS),
    WebDAVProperty("quota", DAV_NS),
    WebDAVProperty("quotaused", DAV_NS),
]


class BasePropFinder(object):
    def __init__(self, filename, url_name):
        self.filename = filename
        self.url_name = url_name
        self.file_stat = os.stat(filename)

    def __getitem__(self, prop):
        try:
            res = getattr(self, prop.replace("-", "_"))()
            if res == None:
                return None
            elif type(res) == ET.Element:
                return res
            return str(res)
        except AttributeError:
            return None

    def _http_time_(self, t):
        # Sun, 06 Nov 1994 08:49:37 GMT
        return time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime(t))

    def quota_available_bytes(self):
        s = os.statvfs(self.filename)
        return s.f_frsize * s.f_bavail

    def quota_used_bytes(self):
        s = os.statvfs(self.filename)
        return s.f_frsize * s.f_blocks - s.f_frsize * s.f_bavail

    def creationdate(self):
        return self._http_time_(self.file_stat.st_ctime)

    def displayname(self):
        return self.url_name

    def getcontentlanguage(self):
        return "en"

    def getcontentlength(self):
        return self.file_stat.st_size

    def getcontenttype(self):
        return None

    def getetag(self):
        return str(self.file_stat.st_mtime).replace(".", "-")

    def getlastmodified(self):
        return self._http_time_(self.file_stat.st_mtime)

    def lockdiscovery(self):
        return ""

    def resourcetype(self):
        return None

    def supportedlock(self):
        return ET.Element("d:supportedlock")


class DirPropFinder(BasePropFinder):
    def resourcetype(self):
        return ET.Element("d:collection")


class FilePropFinder(BasePropFinder):
    def resourcetype(self):
        return ""


class Depth(object):
    DEPTH0 = 0
    DEPTH1 = 1
    DEPTH_INFINITY = "infinity"


class WebDAVCommand(object):
    MKDIR = "mkdir"
    RMDIR = "rmdir"
    RMFILE = "rmfile"
    SAVE = "save"
    COPYFILE = "copyfile"
    COPYDIR = "copydir"
    MOVEFILE = "movefile"  # MOVE method
    MOVEDIR = "movedir"  # MOVE method
    GETPROPNAMES = "getpropnames"
    GETPROPS = "getproperties"
    SETPROPS = "setproperties"  # PROPPATCH method
    RMPROPS = "removeproperties"  # PROPPATCH method
    LOCK = "lock"
    UNLOCK = "unlock"


class WebDAVCommandListener(object):
    def before(self, command, *args):
        # common bewfore handler
        return True

    def after(self, command, result, *args):
        # common after handler
        return result

    ## function handlers

    # def before_<command>(self, *args):
    #     # some activity
    #     return True

    # def instead_<command>(self, original_command_function, *args):
    #     return

    # def after_<command>(self, result, *args):
    #     # some activity
    #     return result


class WebDAVBaseExecutor(object):
    def __init__(self, listener=None):
        if listener != None:
            if not isinstance(listener, WebDAVCommandListener):
                raise TypeError("listener must be of WebDAVCommandListener type")
        self.listener = listener

    def __getfunction(self, obj, name):
        try:
            return getattr(obj, name)
        except AttributeError:
            return None

    def __call__(self, command, *args):
        command_func = self.__getfunction(self, command)
        if not command_func:
            return None
        if self.listener:
            before_func = self.__getfunction(self.listener, "before_%s" % command)
            replace_func = self.__getfunction(self.listener, "replace_%s" % command)
            after_func = self.__getfunction(self.listener, "after_%s" % command)
        if before_func:
            if not before_func(*args):
                return None
        if not self.listener.before(command, *args):
            return None
        if replace_func:
            res = replace_func(command_func, *args)
        else:
            res = command_func(*args)
        if after_func:
            res = after_func(res, *args)
        res = self.listener.after(command, res, *args)
        return res


class WebDAVExecutor(WebDAVBaseExecutor):
    def __init__(self, listener=None):
        WebDAVBaseExecutor.__init__(self, listener)

    def mkdir(self, filename):
        os.mkdir(filename)

    def rmdir(self, filename):
        os.rmdir(filename)

    def rmfile(self, filename):
        os.remove(filename)

    def save(self, filename, infile):
        with open(filename, "wb") as out_file:
            out_file.write(infile.read())

    def copyfile(self, filename, destination):
        shutil.copyfile(filename, destination)

    def copydir(self, filename, destination):
        shutil.copytree(filename, destination)

    def getpropnames(self):
        res = list(filter(lambda x: x[0] != "_", dir(BasePropFinder)))
        for prop in COMMON_PROPS:
            n = prop.name.replace("-", "_")
            if n in res:
                res.remove(n)
        return list(map(lambda p: WebDAVProperty(p, DAV_NS), res))

    def getproperties(self, filename, url_name, props):
        res = dict()
        if os.path.isdir(filename):
            prop_finder = DirPropFinder(filename, url_name)
        else:
            prop_finder = FilePropFinder(filename, url_name)
        for prop in props:
            val = prop_finder[prop.name]
            if val != None:
                res[prop] = val
        return res


class WebDAVServerBase(object):
    STATUS_OK = "HTTP/1.1 200 OK"

    def __init__(self, app, path_root, file_root, executor=None, listener=None):
        self.path_root = path_root
        self.file_root = file_root
        if self.file_root[-1] != os.path.sep:
            self.file_root += os.path.sep
        if executor:
            self.executor = executor
            self.executor.listener = listener
        else:
            self.executor = WebDAVExecutor(listener)
        self.init_routes(app, self.file_root, path_root)

    def file_exists(self, filename):
        return os.path.exists(self.file_root + filename)

    def is_dir(self, filename):
        return os.path.isdir(self.file_root + filename)

    def get_depth(self, request):
        depth = request.get_header("Depth")
        if depth == "0":
            return Depth.DEPTH0
        if depth == "1":
            return Depth.DEPTH1
        return Depth.DEPTH_INFINITY

    def get_files_list_html(self, path, files_list):
        tpl = "<!DOCTYPE html><html>\n<head>\n<title>%s</title>\n</head>\n<body>%s\n%s</body></html>"
        title = "<h3>%s</h3><hr/>\n" % path
        link_tpl = "<a href='%s%s'>%s</a><br/>"
        links = []
        for l in files_list:
            links.append(link_tpl % (path, l, l))
        return tpl % (path, title, "\n".join(links))

    def normalized_files_list(self, path=None):
        if not path:
            path = "."

        def normalize(filename):
            if self.is_dir(filename):
                return "%s/" % filename
            return filename

        return map(normalize, os.listdir(self.file_root + path))

    def normalize_url(self, url):
        if url and url[-1] == "/":
            return url[:-1]
        return url

    def init_routes(self, app, file_root, path_root):
        pass


class WebDAVPropFind(WebDAVServerBase):
    ALLPROP = WebDAVProperty("allprop", DAV_NS)
    PROPNAME = WebDAVProperty("propname", DAV_NS)

    def __init__(self, app, path_root, file_root, executor=None, listener=None):
        WebDAVServerBase.__init__(self, app, path_root, file_root, executor, listener)

    def parse_propfind(self, body):
        res = []
        if len(body) == 0:
            return [self.ALLPROP]
        doc = ET.fromstring(body)
        for el in list(doc):
            if "prop" in el.tag:
                for prop in list(el):
                    res.append(WebDAVProperty.parse(prop.tag))
        return res

    def get_properties(self, filename, url_name, props):
        # print("get_properties('%s', %s)" % (filename, props))
        res = ET.Element("d:propstat", DAV_NS.as_dict())
        if self.ALLPROP in props:
            props = self.executor(WebDAVCommand.GETPROPNAMES)
        elif self.PROPNAME in props:
            res = ET.Element("d:propstat", DAV_NS.as_dict())
            for prop in self.executor(WebDAVCommand.GETPROPNAMES):
                prop.xmlElement(res)
            return res
        for prop, val in self.executor(
            WebDAVCommand.GETPROPS, filename, url_name, props
        ).items():
            item = prop.xmlElement(res)
            if type(val) == ET.Element:
                item.append(val)
            else:
                item.text = val
        return res

    def init_routes(self, app, file_root, path_root):
        @app.route(path_root, method="PROPFIND")
        @app.route(path_root + "/", method="PROPFIND")
        @app.route(path_root + "/<filename:path>", method="PROPFIND")
        def path_propfind(filename=""):
            depth = self.get_depth(bottle.request)
            if depth == Depth.DEPTH_INFINITY:
                bottle.abort(403)
            filename = self.normalize_url(filename)
            if not self.file_exists(filename):
                bottle.abort(404)
            prop_list = self.parse_propfind(bottle.request.body.read())
            bottle.response.content_type = "text/xml"

            def item_prop():
                url = self.normalize_url(path_root + "/" + filename)
                res = ET.Element("d:response", DAV_NS.as_dict())
                ET.SubElement(res, "d:href").text = url
                props = self.get_properties(file_root + filename, url, prop_list)
                res.append(props)
                ET.SubElement(res, "d:status").text = self.STATUS_OK
                return res

            bottle.response.status = 207
            ms = ET.Element("d:multistatus", DAV_NS.as_dict())
            if depth == Depth.DEPTH0:
                ms.append(item_prop())
            elif depth == Depth.DEPTH1:
                if self.is_dir(filename):
                    res = item_prop()
                    ET.SubElement(res, "d:status").text = self.STATUS_OK
                    ms.append(res)
                    x_path_root = path_root
                    x_file_root = file_root
                    if filename:
                        x_path_root = x_path_root + "/" + filename
                        x_file_root = x_file_root + filename + os.path.sep
                    for fn in os.listdir(x_file_root):
                        res = ET.Element("d:response", DAV_NS.as_dict())
                        url = self.normalize_url(x_path_root + "/" + fn)
                        ET.SubElement(res, "d:href").text = url
                        props = self.get_properties(x_file_root + fn, url, prop_list)
                        res.append(props)
                        ET.SubElement(res, "d:status").text = self.STATUS_OK
                        ms.append(res)
                else:
                    ms.append(item_prop())
            xml = ET.tostring(ms)
            # print(xml)
            return xml


class WebDAVPropPatch(WebDAVServerBase):
    def __init__(self, app, path_root, file_root, executor=None, listener=None):
        WebDAVServerBase.__init__(self, app, path_root, file_root, executor, listener)

    def parse_proppatch(self, body):
        def notmalize(tag):
            if "}" in tag:
                return tag.split("}")[1]
            else:
                return tag

        # <D:propertyupdate xmlns:D="DAV:"
        #          xmlns:Z="http://ns.example.com/standards/z39.50/">
        #    <D:set>
        #      <D:prop>
        #        <Z:Authors>
        #          <Z:Author>Jim Whitehead</Z:Author>
        #          <Z:Author>Roy Fielding</Z:Author>
        #        </Z:Authors>
        #      </D:prop>
        #    </D:set>
        #    <D:remove>
        #      <D:prop><Z:Copyright-Owner/></D:prop>
        #    </D:remove>
        # </D:propertyupdate>

        res = []
        # if len(body) == 0:
        #     return [self.ALLPROP]
        # doc = ET.fromstring(body)
        # for el in list(doc):
        #     if "prop" in el.tag:
        #         for prop in list(el):
        #             res.append(notmalize(prop.tag))
        return res

    def init_routes(self, app, file_root, path_root):
        @app.route(path_root, method="PROPATCH")
        @app.route(path_root + "/", method="PROPATCH")
        @app.route(path_root + "/<filename:path>", method="PROPATCH")
        def path_proppatch(filename=""):
            filename = self.normalize_url(filename)
            if not self.file_exists(filename):
                bottle.abort(404)
            bottle.abor(403)


class WebDAVServer(WebDAVServerBase):
    def __init__(self, app, path_root, file_root, executor=None, listener=None):
        WebDAVServerBase.__init__(self, app, path_root, file_root, executor, listener)
        WebDAVPropFind(app, path_root, file_root, executor, listener)
        WebDAVPropPatch(app, path_root, file_root, executor, listener)

    def init_routes(self, app, file_root, path_root):
        @app.route(path_root + "/", method="OPTIONS")
        @app.route(path_root + "/<filename:path>", method="OPTIONS")
        def all_options():
            bottle.response.set_header("DAV", "1, 2")
            return ""

        @app.route(path_root + "/", method="HEAD")
        def root_head():
            return ""

        @app.route(path_root + "/<filename:path>", method="HEAD")
        def file_head(filename):
            if self.file_exists(filename):
                return ""
            else:
                bottle.abort(404)

        @app.route(path_root + "/", method="GET")
        def root_get():
            root = "%s/" % path_root
            return self.get_files_list_html(root, self.normalized_files_list())

        @app.route(path_root + "/<filename:path>", method="GET")
        def file_get(filename):
            if self.file_exists(filename):
                if self.is_dir(filename):
                    root = "%s/%s" % (path_root, filename)
                    return self.get_files_list_html(
                        root, self.normalized_files_list(filename)
                    )
                else:
                    return bottle.static_file(filename, file_root)
            else:
                bottle.abort(404)

        @app.route(path_root + "/<filename:path>", method="MKCOL")
        def path_mkdir(filename):
            if self.file_exists(filename):
                bottle.abort(405)
            if "/" in filename[:-1]:
                head = "/".join(filename[:-1].split("/")[:-1])
                if not self.file_exists(head):
                    bottle.abort(409)
            self.executor(WebDAVCommand.MKDIR, file_root + filename)
            bottle.abort(201)

        @app.route(path_root + "/<filename:path>", method="DELETE")
        def path_delete(filename):
            if not self.file_exists(filename):
                bottle.abort(404)
            if self.is_dir(filename):
                depth = self.get_depth(bottle.request)
                if depth == Depth.DEPTH_INFINITY:
                    self.executor(WebDAVCommand.RMDIR, file_root + filename)
                    bottle.abort(204)
                else:
                    bottle.abort(409)
            else:
                self.executor(WebDAVCommand.RMFILE, file_root + filename)
                bottle.abort(204)

        @app.route(path_root + "/<filename:path>", method="PUT")
        def path_put(filename=""):
            filename = self.normalize_url(filename)
            if "/" in filename:
                file_path = "/".join(filename.split("/")[:-1])
                if not self.file_exists(file_path):
                    bottle.abort(409)
            self.executor(WebDAVCommand.SAVE, file_root + filename, bottle.request.body)
            bottle.abort(201)

        @app.route(path_root + "/<filename:path>", method="COPY")
        def path_copy(filename=""):
            filename = self.normalize_url(filename)
            depth = self.get_depth(bottle.request)
            # get normalized destination
            destination = urllib.parse.urlparse(
                bottle.request.get_header("Destination")
            ).path
            root_index = destination.find(path_root)
            if root_index <= 1:
                destination = destination.split(path_root, 1)[1]
            if destination[0] == "/":
                destination = destination[1:]
            # check destination folder
            if "/" in destination:
                dest_path = "/".join(destination.split("/")[:-1])
                if not self.file_exists(dest_path):
                    # if destination path does not exist
                    bottle.abort(409, "Destination path does not exist")
            # check is this copy file or path
            if not self.file_exists(filename):
                bottle.abort(409, "Source file does not exist")
            if self.is_dir(filename):
                # path copy mode
                if depth == Depth.DEPTH0:
                    self.executor(WebDAVCommand.MKDIR, file_root + destination)
                elif depth == Depth.DEPTH_INFINITY:
                    self.executor(
                        WebDAVCommand.COPYDIR,
                        file_root + filename,
                        file_root + destination,
                    )
                else:
                    bottle.abort(400, "Depth = 1 not supported. (see RFC4918 #9.8.3.)")
            else:
                # file copy mode
                if not self.file_exists(destination):
                    # if file already exist
                    self.executor(
                        WebDAVCommand.COPYFILE,
                        file_root + filename,
                        file_root + destination,
                    )
            return ""
